﻿using System;
using System.Linq;

namespace StringCalculatorKata
{
    public class StringCalculator
    {
        private const int _defaultValue = 0;
        private const int _maxValue = 1000;
        private const char _defaultSeparator = ',';

        private NumbersSanitizer _numbersSanitizer;

        public StringCalculator()
        {
            _numbersSanitizer = new NumbersSanitizer(_defaultSeparator);
        }

        public int Add(string numbers)
        {
            if (ShouldReturnDefaultNumber(numbers))
                return _defaultValue;

            numbers = _numbersSanitizer.SanitizeNumbers(numbers);
            int[] numbersArray = ConvertToNumbersArray(numbers);

            CheckNegativeNumbers(numbersArray);
            numbersArray = IgnoreNumbersExceedingMaxValue(numbersArray);

            return numbersArray.Sum();
        }

        private static int[] IgnoreNumbersExceedingMaxValue(int[] numbersArray)
        {
            return numbersArray.Where(x => x <= _maxValue).ToArray();
        }

        private static void CheckNegativeNumbers(int[] numbersArray)
        {
            if (numbersArray.Any(x => x < 0))
                throw new ArgumentException(String.Format("Negatives not allowed: {0}", JoinNumbersToString(numbersArray)));
        }

        private static string JoinNumbersToString(int[] numbersArray)
        {
            return String.Join(",", numbersArray.Where(x => x < 0));
        }

        private static int[] ConvertToNumbersArray(string numbers)
        {
            string[] splitNumbers = SplitNumbers(numbers);

            return splitNumbers.Select(ConvertSingleNumber).ToArray();
        }

        private static string[] SplitNumbers(string numbers)
        {
            return numbers.Split(_defaultSeparator);
        }

        private static int ConvertSingleNumber(string number)
        {
            return Int32.Parse(number);
        }

        private static bool ShouldReturnDefaultNumber(string numbers)
        {
            return String.IsNullOrWhiteSpace(numbers);
        }
    }
}
