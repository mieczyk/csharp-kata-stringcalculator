﻿using System.Text.RegularExpressions;

namespace StringCalculatorKata
{
    public class NumbersSanitizer
    {
        private readonly char _defaultSeparator;
        public NumbersSanitizer(char defaultSeparator)
        {
            _defaultSeparator = defaultSeparator;
        }

        public string SanitizeNumbers(string numbers)
        {
            if (ContainsSeparatorSpecifier(numbers))
            {
                string separator = ExtractSeparator(numbers);

                if (MultipleSeparatorsDefined(separator))
                    numbers = ReplaceMultipleSeparatorsWithDefaultSeparator(numbers);
                else
                    numbers = ReplaceWithDefaultSeparator(numbers, separator);

                numbers = RemoveSeparatorSpecifier(numbers);
            }

            return ReplaceWithDefaultSeparator(numbers, "\n");
        }

        private string ReplaceMultipleSeparatorsWithDefaultSeparator(string numbers)
        {
            MatchCollection matches = Regex.Matches(numbers, @"\[([^\[\]]+)\]");
            
            foreach (Match match in matches)
            {
                if (match.Success)
                    numbers = ReplaceWithDefaultSeparator(numbers, match.Groups[1].Value);
            }

            numbers = numbers.Replace("[,]", "");
            
            return numbers;
        }

        private static bool MultipleSeparatorsDefined(string separatorSpecifier)
        {
            return separatorSpecifier.Contains("[");
        }

        private static string ExtractSeparator(string numbers)
        {
            string separator = numbers[2].ToString();

            Match match = Regex.Match(numbers, @"//(.+)\n");
            if (match.Success)
                separator = match.Groups[1].Value;
            
            return separator;
        }

        private static string RemoveSeparatorSpecifier(string numbers)
        {
            int index = 3;

            if (numbers.Contains("//,\n"))
                index = 4;
            
            return numbers.Substring(index);
        }

        private string ReplaceWithDefaultSeparator(string numbers, string oldSeparator)
        {
            return numbers.Replace(oldSeparator, _defaultSeparator.ToString());
        }

        private static bool ContainsSeparatorSpecifier(string numbers)
        {
            return numbers.StartsWith("//");
        }
    }
}
