﻿using NUnit.Framework;

namespace StringCalculatorKata.Tests
{
    [TestFixture]
    public class NumberSanitizerTest
    {
        [TestCase("1,2\n5", "1,2,5")]
        [TestCase("1,2\n5,2", "1,2,5,2")]
        [TestCase("1\n2\n5\n2", "1,2,5,2")]
        public void SanitizeNumbers_WhenNumbersSplitWithNewLine_ReturnsSanitizedString(string numbers, string expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        [TestCase("//;\n1;2;3", "1,2,3")]
        [TestCase("//&\n1&2&3", "1,2,3")]
        [TestCase("//^\n1^2^3^4", "1,2,3,4")]
        public void SanitizeNumbers_WhenContainsSeparatorSpecifier_ReturnsSanitizedString(string numbers, string expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        [TestCase("//***\n1***2***3", "1,2,3")]
        [TestCase("//^&*\n4^&*5^&*5", "4,5,5")]
        [TestCase("//tytus\n4tytus5tytus5tytus6tytus8", "4,5,5,6,8")]
        public void SanitizeNumbers_WhenContainsSeparatorWithVariableLength_ReturnSanitizedString(string numbers, string expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        [TestCase("//[;][*]\n1;2*3", "1,2,3")]
        [TestCase("//[&][^][x]\n1^2x3&8", "1,2,3,8")]
        [TestCase("//[^][e]\n1e2^3e4", "1,2,3,4")]
        public void SanitizeNumbers_WhenContainsMoreThanOneSeparator_ReturnsSanitizedString(string numbers, string expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        [TestCase("//[;][***]\n1;2***3", "1,2,3")]
        [TestCase("//[&%][^][xx]\n1^2xx3&%8", "1,2,3,8")]
        [TestCase("//[^^][ert]\n1ert2^^3ert4", "1,2,3,4")]
        public void SanitizeNumbers_WhenContainsMoreThanOneSeparatorWithVariableLength_ReturnsSanitizedString(string numbers, string expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        private static void ArrangeActAndAssert(string numbers, string expected)
        {
            NumbersSanitizer numbersSanitizer = new NumbersSanitizer(',');
            string result = numbersSanitizer.SanitizeNumbers(numbers);

            Assert.AreEqual(expected, result);
        }
    }

}
