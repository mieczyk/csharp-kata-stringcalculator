﻿using NUnit.Framework;
using System;

namespace StringCalculatorKata.Tests
{
    [TestFixture]
    public class StringCalculatorTest
    {
        [Test]
        public void Add_WhenEmptyString_ReturnsZero()
        {
            ArrangeActAndAssert(String.Empty, 0);
        }

        [Test]
        public void Add_WhenWhiteSpaceCharacters_ReturnsZero()
        {
            ArrangeActAndAssert(" \t\n", 0);
        }

        [TestCase("1", 1)]
        [TestCase("3", 3)]
        [TestCase("12", 12)]
        public void Add_WhenSingleNumber_ReturnsThatNumber(string numbers, int expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        [TestCase("1,2", 3)]
        [TestCase("2,5", 7)]
        [TestCase("12,13", 25)]
        [TestCase("1000,5", 1005)]
        public void Add_WhenMultipleNumbers_ReturnsSumOfNumbers(string numbers, int expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        [TestCase("1,2,3", 6)]
        [TestCase("1,2,3,4", 10)]
        [TestCase("2,3,2,2,6", 15)]
        public void Add_WhenUnknownAmountOfNumbers_ReturnsSumOfNumbers(string numbers, int expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        [TestCase("-1,1,2", ExpectedException = typeof(ArgumentException), ExpectedMessage = "Negatives not allowed: -1" )]
        [TestCase("-1,-2,-1,6", ExpectedException = typeof(ArgumentException), ExpectedMessage = "Negatives not allowed: -1,-2,-1")]
        public void Add_WhenNegativeNumber_ThrowsExceptionWithMessage(string numbers)
        {
            ArrangeActAndAssert(numbers, 2);
        }

        [Test]
        [TestCase("1001,2", 2)]
        [TestCase("2000,4,5", 9)]
        [TestCase("1233,1234,10,15", 25)]
        public void Add_WhenNumberIsGreaterThanOneThousand_IgnoresThatNumber(string numbers, int expected)
        {
            ArrangeActAndAssert(numbers, expected);
        }

        private static void ArrangeActAndAssert(string numbers, int expected)
        {
            StringCalculator stringCalculator = new StringCalculator();
            int result = stringCalculator.Add(numbers);
            
            Assert.AreEqual(expected, result);
        }
    }
}
